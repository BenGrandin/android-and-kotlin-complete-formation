package com.example.helloworld

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

class TestRelativeLayout : AppCompatActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_test_relative_layout)
	}
}
