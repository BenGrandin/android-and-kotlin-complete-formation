package com.example.helloworld

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_user_detail_activity.*

class UserDetailActivity : AppCompatActivity() {
	companion object {
		private val TAG = UserDetailActivity::class.java.simpleName
		//private val TAG = UserDetailActivity::class.qualifiedName
	}


	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_user_detail_activity)

		/**
		 * Get action from other activity
		 */
		/*
		val action = intent.action
		val isUserViewer = intent.hasCategory("UserViewer")

		intent.extras?.let { b ->
			//val extras: Bundle = intent.extras!!
			val name = b.getString("name")
			val age = b.getInt("age")
			Log.i(TAG, "action: $action name: $name age: $age ")
		}
		*/

		val user = intent.getParcelableExtra<User>("user")

		//val text = getString(user.name)

		name.text = "Nom: ${user.name}"
		age.text = "Age: ${user.age}"
	}
}
