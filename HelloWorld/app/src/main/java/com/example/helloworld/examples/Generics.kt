package com.example.helloworld.examples

import java.lang.StringBuilder

/**
 * Function
 */
fun <T> printArray(array: Array<T>) {
	var separator = ""
	val sb = StringBuilder()

	for (i in array.indices.reversed()) {
		sb.append(separator)
		sb.append(array[i])

		separator = ", "
	}

	println(sb.toString())
}


/**
 * Class
 */
class Box<T>(var value: T) {
	fun set(newValue: T) {
		value = newValue;
	}

	fun get(): T {
		return value
	}
}

fun generics() {

	// Function
	val integers = arrayOf(1, 2, 3, 4)
	val strings = arrayOf("Hello", "World", "en", "Kotlin")

	printArray(integers)
	printArray(strings)

	// Class

	val boxInt = Box<Int>(2)
	val v: Int = boxInt.get()
	println("Box Value: $v")

	val boxString = Box<String>("Kotlin")
	println("Box content ${boxString.get()}")
	boxString.set("Rocks")
	println("Box content ${boxString.get()}")
}