package com.example.helloworld.examples

class User(val name: String,val age:Int){
	lateinit var nickname:String
}

fun lateinit(){
	val bob = User("Bob", 10)
	bob.nickname = "MJ"
	println("Surnom de ${bob.name} est ${bob.nickname}")
}