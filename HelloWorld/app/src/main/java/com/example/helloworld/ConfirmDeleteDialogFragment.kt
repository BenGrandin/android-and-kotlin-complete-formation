package com.example.helloworld

import android.app.AlertDialog
import android.app.Dialog
import android.content.DialogInterface
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager

class ConfirmDeleteDialogFragment : DialogFragment() {
	companion object {
		private val TAG = ConfirmDeleteDialogFragment::class.java.simpleName
	}

//	override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
//		//ToDo: D'ou vient AlertDialog ?
//		val builder = AlertDialog.Builder(activity)
//
//		builder
//			.setTitle("Delete entry")
//			.setMessage("Supprimer tout le contenu du téléphone ?")
//			.setPositiveButton("Oh oui !", object : DialogInterface.OnClickListener {
//				override fun onClick(dialog: DialogInterface?, which: Int) {
//					Log.i(TAG, "Youpi ! On va tout casser")
//				}
//			})
//			.setNegativeButton("Euh.. Non", DialogInterface.OnClickListener { dialog, which ->
//				Log.i(TAG, "Ce sera pour la prochaine fois")
//			})
//		return builder.create()
//	}


	override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
		return activity?.let {
			// Use the Builder class for convenient dialog construction
			val builder = AlertDialog.Builder(it)
			builder.setMessage("Supprimer tout le contenu du téléphone ?")
				.setPositiveButton("Oh oui !",
					DialogInterface.OnClickListener { dialog, id ->
						Log.i(TAG, "Youpi ! On va tout casser")
					})
				.setNegativeButton("Euh.. Non !",
					DialogInterface.OnClickListener { dialog, id ->
						Log.i(TAG, "Ce sera pour la prochaine fois")
					})
			// Create the AlertDialog object and return it
			builder.create()
		} ?: throw IllegalStateException("Activity cannot be null")
	}


}

